﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MyStack<T> where T : IComparable

    {
        private T[] arr = null;
        private int currentIndex = 0;
        private T max;

        public MyStack() : this(100) { }

        public MyStack(int size)
        {
            arr = new T[size];
        }
        public void Push(T item)
        {
            if (currentIndex >= arr.Length) { throw new StackOverflowException(); }

            arr[currentIndex] = item;

            currentIndex++;
        }
        public T Pop()
        {

            if (currentIndex <= 0)
            {
                throw new Exception("stack is empty.");
            }
            return arr[currentIndex--];

        }
        public void PushToBottom(T item)
        {
            if (currentIndex == 0)
            {
                Push(item);
            }
            else
            {
                T tempItem = Pop();
                PushToBottom(item);
                Push(tempItem);
            }
        }
        public T Top()
        {
            if (currentIndex == 0)
            {
                throw new Exception("stack is empty.");
            }
            return arr[currentIndex];
        }
        //max for O(n)
        public T Max()
        {
            if (currentIndex == 0)
            {
                return max;
            }
            else
            {
                T tempItem = Pop();
                if (tempItem.CompareTo(max) > 0)
                {
                    max = tempItem;
                }
                Max();
                Push(tempItem);
                return max;

            }

        }
    }

        class Program
        {

            static void Main(string[] args)
            {


                try
                {
                    MyStack<int> stack = new MyStack<int>(10);
                    stack.Push(150);
                    stack.Push(84);
                    stack.Push(9);
                    stack.Push(500);
                    stack.Push(0);
                    stack.Max();
                    Console.WriteLine(stack.Max());



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
             Console.ReadLine();
            }
        }
    }
